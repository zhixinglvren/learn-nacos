package com.learn.nacos;

import com.alibaba.nacos.api.exception.NacosException;
import com.alibaba.nacos.api.naming.NamingFactory;
import com.alibaba.nacos.api.naming.NamingService;

public class Provider {

    public static void main(String[] args) {
        try {
            String serviceName = "nacos.springboot.service";
            NamingService namingService = NamingFactory.createNamingService("127.0.0.1:8848");
            namingService.registerInstance(serviceName, "127.0.0.1", 8080);
            Thread.sleep(Integer.MAX_VALUE);
        } catch (NacosException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
