package com.learn.nacos;

import com.alibaba.nacos.api.exception.NacosException;
import com.alibaba.nacos.api.naming.NamingFactory;
import com.alibaba.nacos.api.naming.NamingService;
import com.alibaba.nacos.api.naming.listener.NamingEvent;

public class Consumer {

    public static void main(String[] args) {
        try {
            String serviceName = "nacos.springboot.service";
            NamingService namingService = NamingFactory.createNamingService("127.0.0.1:8848");
            namingService.subscribe(serviceName, event -> {
                if (event instanceof NamingEvent) {
                    System.out.println("订阅到数据");
                    System.out.println(((NamingEvent) event).getInstances());
                }
            });
            System.out.println("订阅完成，准备等数据来");
            Thread.sleep(Integer.MAX_VALUE);
        } catch (NacosException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
